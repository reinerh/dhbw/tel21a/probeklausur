package main

/* AUFGABENSTELLUNG:
Schreiben Sie eine Funktion zip(), die zwei Strings erwartet.
Die Funktion soll die beiden Strings zusammenmischen, indem Sie
immer abwechselnd ein Zeichen aus dem einen und dem anderen nimmt.
Sind die Strings nicht gleich lang, soll der Rest des längeren ans
Ende gehängt werden.
*/
func zip(s1, s2 string) string {
	result := ""

	// Länge des kürzeren Strings bestimmen:
	minLaenge := len(s1)
	if len(s2) < len(s1) {
		minLaenge = len(s2)
	}

	// Gemeinsamen Teil von s1 und s2 abarbeiten:
	for i := 0; i < minLaenge; i++ {
		result += string(s1[i]) + string(s2[i])
	}

	// Den Rest von beiden Strings ans Ergebnis anhängen.
	// Anmerkung: Eine dieser beiden Slices ist leer, deshalb können wir bedenkenlos
	//            ohne weitere Überprüfung beide anhängen.
	result += s1[minLaenge:]
	result += s2[minLaenge:]

	return result
}

/* Elegante rekursive Alternative: */
func zip2(s1, s2 string) string {
	if s1 == "" {
		return s2
	}
	if s2 == "" {
		return s1
	}
	return s1[:1] + s2[:1] + zip2(s1[1:], s2[1:])
}
