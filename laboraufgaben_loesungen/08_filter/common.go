package main

/* VORGABE:
Die folgende Funktion filter() erwartet eine int-Slice und ein Prädikat p(int) bool.
Das Prädikat liefert für jedes Element x in der Liste p(x) == true oder p(x) == false.
filter() liefert die Liste, die nur noch die Elemente x enthält, für die p(x) == true liefert.
*/
func filter(list []int, p func(int) bool) []int {
	result := make([]int, 0)
	for _, v := range list {
		if p(v) {
			result = append(result, v)
		}
	}
	return result
}

/* AUFGABENSTELLUNG:
Schreiben Sie eine Funktion common(), die als Parameter zwei int-Slices l1 und l2 erwartet.
Die Funktion soll eine int-Slice liefern, die nur noch die Elemente x enthält, die in beiden Listen vorkommen.
Verwenden Sie dafür die obige  Funktion filter(). Verwenden Sie keine Schleife und keine Rekursion!
*/
func common(l1, l2 []int) []int {
	l2ContainsX := func(x int) bool {
		yEqualsX := func(y int) bool { return y == x }
		return len(filter(l2, yEqualsX)) > 0
	}
	return filter(l1, l2ContainsX)
}
