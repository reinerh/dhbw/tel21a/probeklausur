package main

/* AUFGABENSTELLUNG:
Schreiben Sie eine rekursive Funktion power2(), die einen int-Parameter x erwartet.
Die Funktion soll die Potenz "2 hoch x" berechnen und zurückliefern.
*/
func power2(x int) int {
	// Rekursionsanker: 2^0 == 1
	if x == 0 {
		return 1
	}
	// Allgemeiner Fall: 2^x == 2^(x-1) * 2
	return power2(x-1) * 2
}
