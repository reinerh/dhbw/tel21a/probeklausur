package main

/* AUFGABENSTELLUNG:
Schreiben Sie eine Funktion arraySums(), die als Parameter eine int-Slice l erwartet.
Die Funktion soll eine int-Slice liefern, die an Stelle n die Summe der Elemente aus l bis zu Stelle n enthält.
*/
func arraySums(l []int) []int {
	result := make([]int, 0)
	currentSum := 0
	for _, v := range l {
		currentSum += v
		result = append(result, currentSum)
	}
	return result
}
