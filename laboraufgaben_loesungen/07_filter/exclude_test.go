package main

import "fmt"

// Gibt Ergebnisse von sums() auf die Konsole aus.
// Der Kommentar unten gibt die erwarteten Ergebnisse an.
// Automatische Prüfung mittels des Befehls "go test" (statt "go run").
func ExampleExclude() {
	p1 := func(x int) bool { return x%2 == 0 }

	fmt.Println(exclude([]int{1, 3, 5, 7}, p1))  // Soll [1 3 5 7] ausgeben.
	fmt.Println(exclude([]int{1, 1, 2, 80}, p1)) // Soll [1 1] ausgeben.
	fmt.Println(exclude([]int{7, 3, 1, 2}, p1))  // Soll [7 3 1] ausgeben.
	fmt.Println(exclude([]int{3, 3, 0, 2}, p1))  // Soll [3 3] ausgeben.
	fmt.Println(exclude([]int{2, 4, 6, 8}, p1))  // Soll [] ausgeben.
	// Output:
	// [1 3 5 7]
	// [1 1]
	// [7 3 1]
	// [3 3]
	// []
}
