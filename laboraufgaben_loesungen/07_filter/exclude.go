package main

/* VORGABE:
Die folgende Funktion filter() erwartet eine int-Slice und ein Prädikat p(int) bool.
Das Prädikat liefert für jedes Element x in der Liste p(x) == true oder p(x) == false.
filter() liefert die Liste, die nur noch die Elemente x enthält, für die p(x) == true liefert.
*/
func filter(list []int, p func(int) bool) []int {
	result := make([]int, 0)
	for _, v := range list {
		if p(v) {
			result = append(result, v)
		}
	}
	return result
}

/* AUFGABENSTELLUNG:
Schreiben Sie eine Funktion exclude(), die als Parameter eine int-Slice l und ein Prädikat p erwartet.
Die Funktion soll eine int-Slice liefern, die nur noch die Elemente x enthält, für die p(x) == false gilt.
Verwenden Sie dafür die obige  Funktion filter(). Verwenden Sie keine Schleife und keine Rekursion!
*/
func exclude(l []int, p func(int) bool) []int {
	return filter(l, func(v int) bool { return !p(v) })
}
