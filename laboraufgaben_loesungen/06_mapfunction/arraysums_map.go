package main

/* VORGABE:
Die folgende Funktion erwartet eine int-Slice und eine Funktion f(int) int.
Sie bildet jedes Element x in der Liste auf f(x) und liefert die Ergebnisliste.
*/
func mapList(list []int, f func(int) int) []int {
	result := make([]int, len(list))
	for i, v := range list {
		result[i] = f(v)
	}
	return result
}

/* AUFGABENSTELLUNG:
Schreiben Sie eine Funktion arraySums(), die als Parameter eine int-Slice l erwartet.
Die Funktion soll eine int-Slice liefern, die an Stelle n die Summe der Elemente aus l bis zu Stelle n enthält.
Verwenden Sie dafür die obige  Funktion mapList(). Verwenden Sie keine Schleife und keine Rekursion!
*/
func arraySums(l []int) []int {
	currentSum := 0
	return mapList(l, func(v int) int {
		currentSum += v
		return currentSum
	})
}
