package main

/* AUFGABENSTELLUNG:
Schreiben Sie eine Funktion multfive(), die eine Liste von Zahlen erwartet.
Die Funktion soll das Produkt der Elemente an den durch 5 teilbaren Positionen zurückliefern.
*/
func multfive(liste []int) int {
	result := 1
	for i, v := range liste {
		if i%5 == 0 {
			result *= v
		}
	}
	return result
}

/* Alternative Variante: */
func multfive2(liste []int) int {
	result := 1
	for i := 0; i < len(liste); i += 5 {
		result *= liste[i]
	}
	return result
}
