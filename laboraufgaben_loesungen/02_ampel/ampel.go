package main

/* AUFGABENSTELLUNG:
Schreiben Sie eine Funktion colour(), die eine Zeitangabe t in Sekunden erwartet.
Die Funktion soll einen String zurückgeben, der sagt, welche Farbe eine Ampel
nach t Sekunden hat, wenn sie bei Rot startet und alle 10 Sekunden umschaltet.
*/
func colour(t int) string {
	colours := []string{"Rot", "Rot-Gelb", "Grün", "Gelb"}
	return colours[t/10%4]
}

/* Anmerkung zur Rechnung:
   Mit t/10 rechnen wir aus, wie oft die Ampel zu diesem Zeitpunkt bereits umgeschaltet
   hat. Rechnen wir dieses Ergebnis %4, erhalten wir die Nummer der Phase, in der sich
   die Ampel dann befinden muss.
*/

/* Geschwätzigere Variante: */
func colour2(t int) string {
	t %= 40 // t auf den Bereich eines Ampel-Durchlaufs herunterrechnen.

	if t < 10 {
		return "Rot"
	}
	if t < 20 {
		return "Rot-Gelb"
	}
	if t < 30 {
		return "Grün"
	}
	return "Gelb"
}
